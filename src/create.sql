
create database centraldb;
use centraldb;
CREATE TABLE address (
    id int NOT NULL AUTO_INCREMENT,
    city varchar(64) NOT NULL,
    state varchar(64) NOT NULL,
    country varchar(64) NOT NULL,
    street varchar(64) NOT NULL,
    streetNumber varchar(8) NOT NULL,
    houseNumber varchar(4) NOT NULL,
    postalCode varchar(8) NOT NULL,
    CONSTRAINT address_pk PRIMARY KEY (id)
);

-- Table: order
CREATE TABLE sale (
    id int NOT NULL AUTO_INCREMENT,
    orderDate date NOT NULL,
    status bool NOT NULL,
	productId int NOT NULL,
    quantity int,
    price decimal(10,2),
    finalPrice decimal(10,2),
    discount decimal(10,2),
    productName varchar(64),
    CONSTRAINT order_pk PRIMARY KEY (id)
);


-- Table: product
CREATE TABLE product (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL UNIQUE,
    quantity int NOT NULL,
    price decimal(10,2) NOT NULL,
    hurtPrice decimal(10,2),
    supplierPrice decimal(10,2),
    productCategoryId int NOT NULL,
    description varchar(300) NOT NULL,
    sync bool,
	shopId integer,
    CONSTRAINT product_pk PRIMARY KEY (id)
);

-- Table: productCategory
CREATE TABLE productCategory (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL UNIQUE,
    description varchar(256),
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table: shop
CREATE TABLE shop (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(32) NOT NULL,
    addressId int,
    CONSTRAINT shop_pk PRIMARY KEY (id)
);

-- Table: user
CREATE TABLE user (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(32) NULL,
    password char(64) NULL,
    enabled bool NULL DEFAULT 1,
    userRoleId int NOT NULL,
    CONSTRAINT user_pk PRIMARY KEY (id)
);

-- Table: userRole
CREATE TABLE userRole (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    CONSTRAINT userRole_pk PRIMARY KEY (id)
);



-- Reference: order_shop (table: order)
ALTER TABLE sale ADD CONSTRAINT sale_product FOREIGN KEY (productId)
    REFERENCES product (id);

-- Reference: product_productCategory (table: product)
ALTER TABLE product ADD CONSTRAINT product_productCategory FOREIGN KEY product_productCategory (productCategoryId)
    REFERENCES productCategory (id);
    
ALTER TABLE product ADD CONSTRAINT product_shop FOREIGN KEY (shopId)
    REFERENCES shop(id);

-- Reference: shop_address (table: shop)
ALTER TABLE shop ADD CONSTRAINT shop_address FOREIGN KEY shop_address (addressId)
    REFERENCES address (id);

-- Reference: user_userRole (table: user)
ALTER TABLE user ADD CONSTRAINT user_userRole FOREIGN KEY user_userRole (userRoleId)
    REFERENCES userRole (id);

-- End of file.

	INSERT INTO userRole(name) values('ADMIN');
  INSERT INTO userRole(name) values('USER');
  INSERT INTO userRole(name) values('APP');
  
    INSERT INTO shop(name) values('ZAKOPANE_GUBALOWKA');


  INSERT INTO user(username,password,enabled, userRoleId) values('Maximo','$2a$10$/Hr7R9d.n7ItZlFizXl/BebtIIT7s9eJRDK2Gvc1cO65jTbKaYtu2',1,1);
  INSERT INTO user(username,password,enabled, userRoleId) values('Rambo','$2a$10$jTIqKomUoxPNuMYA2QXPOOoaGYKIxJLjlta4TNZNmq1O0tdNeSmPW',1,2);
  INSERT INTO user(username,password,enabled, userRoleId) values('application','123456',1,3);


  
  select * from userRole;
  select * from user;
  select * from product;
  select * from productcategory;
  
  
  use centraldb;
  select * from product;
  use centraldb_branch;
  select * from product;