package com.maxim.spring.dto;

import javax.validation.constraints.NotNull;

public class ProductDTO {
	
	private String id;
	@NotNull
	private String name;
	@NotNull
	private String category;
	@NotNull
	private String productDescription;
	@NotNull
	private String price;
	@NotNull
	private String supplierPrice;
	@NotNull
	private String hurtPrice;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getSupplierPrice() {
		return supplierPrice;
	}
	public void setSupplierPrice(String supplierPrice) {
		this.supplierPrice = supplierPrice;
	}
	public String getHurtPrice() {
		return hurtPrice;
	}
	public void setHurtPrice(String hurtPrice) {
		this.hurtPrice = hurtPrice;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	
}
