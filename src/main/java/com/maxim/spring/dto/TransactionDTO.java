package com.maxim.spring.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class TransactionDTO {
	private String id;
	private String productName;
	private String shopName;
	private String price;
	private String finalPrice;
//	private Integer quantity;
	private String category;
	private String year;
	private String month;
	private String day;
	private String discount;
	private String date;
	private String productSupplierPrice;
	private String productId;
//	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	private Date transactionDate;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
//	public Integer getQuantity() {
//		return quantity;
//	}
//	public void setQuantity(Integer quantity) {
//		this.quantity = quantity;
//	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
//	public Date getTransactionDate() {
//		return transactionDate;
//	}
//	public void setTransactionDate(Date transactionDate) {
//		this.transactionDate = transactionDate;
//	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductSupplierPrice() {
		return productSupplierPrice;
	}
	public void setProductSupplierPrice(String productSupplierPrice) {
		this.productSupplierPrice = productSupplierPrice;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	
	
	
	
	
	
	
}
