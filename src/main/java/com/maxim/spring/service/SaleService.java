package com.maxim.spring.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maxim.spring.dao.ProductDao;
import com.maxim.spring.dao.SaleDao;
import com.maxim.spring.dto.TransactionDTO;
import com.maxim.spring.model.Product;
import com.maxim.spring.model.Sale;

@Service
public class SaleService {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private SaleDao saleDao;
		public void mapTransactionDTOtoSale(TransactionDTO transactionDTO, Sale sale) {
			
			String pattern = "MM/dd/yyyy";
			DateFormat format = new SimpleDateFormat(pattern);
			
			String dateFromFields = transactionDTO.getMonth()+"/"+transactionDTO.getDay()+"/"+transactionDTO.getYear();
			System.out.println(dateFromFields);
			try {
				
				Product product = productDao.findProductWithName(transactionDTO.getProductName());
				BigDecimal discount;
				System.out.println("1");
				if(transactionDTO.getDiscount()!=null && transactionDTO.getDiscount().isEmpty()==false) {
					System.out.println("inside");
					System.out.println(transactionDTO.getDiscount());
				discount = new BigDecimal(transactionDTO.getDiscount());
				} else {System.out.println("2");
				discount = new BigDecimal("0");	
				}System.out.println("3");
				Date date = format.parse(dateFromFields);
				sale.setOrderDate(date);
				sale.setDiscount(discount);
				sale.setPrice(product.getPrice());
				sale.setProduct(product);
				sale.setQuantity(1);
				sale.setPrice(product.getPrice());
				sale.setFinalPrice(product.getPrice().subtract(discount));
				sale.setProductName(product.getName());
				saleDao.save(sale);
			
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void mapFromSaleToTransaction(Sale sale, TransactionDTO transactionDTO) {
			transactionDTO.setId(new Integer(sale.getId()).toString());
			transactionDTO.setProductName(sale.getProductName());
			transactionDTO.setShopName(sale.getProduct().getShop().getName());
			transactionDTO.setPrice(sale.getPrice().toString());
			transactionDTO.setFinalPrice(sale.getFinalPrice().toString());
			transactionDTO.setDiscount(sale.getDiscount().toString());
			String fullDate = sale.getOrderDate().toString();
			transactionDTO.setDate(fullDate);
			transactionDTO.setProductSupplierPrice(sale.getProduct().getSupplierPrice().toString());
			transactionDTO.setProductId(sale.getProduct().getId().toString());
			
			
		}
		
		public void mapFromSaleToTransactionForUser(Sale sale, TransactionDTO transactionDTO) {
			transactionDTO.setId(new Integer(sale.getId()).toString());
			transactionDTO.setProductName(sale.getProductName());
			transactionDTO.setShopName(sale.getProduct().getShop().getName());
			transactionDTO.setPrice(sale.getPrice().toString());
			transactionDTO.setFinalPrice(sale.getFinalPrice().toString());
			transactionDTO.setDiscount(sale.getDiscount().toString());
			String fullDate = sale.getOrderDate().toString();
			transactionDTO.setDate(fullDate);
			
		}
		
		public List<TransactionDTO> mapFromSalesToTransactions(List<Sale> sales, boolean isAdmin) {

			List<TransactionDTO> transactionsDTO = new ArrayList<>();
			for(Sale sale : sales) {
				TransactionDTO transactionDTO = new TransactionDTO();
				if(isAdmin) {
					mapFromSaleToTransaction(sale, transactionDTO);
				} else {
					mapFromSaleToTransactionForUser(sale, transactionDTO);
				}
				transactionsDTO.add(transactionDTO);
				
			}
			return transactionsDTO;
		}
// deprecated	
//		public Product findProductInList(Integer id, List<Product> products) {
//			for(Product product : products) {
//				if(product.getId().equals(id) ) {
//					return product;
//				}
//			}
//			return null;
//		}
}
