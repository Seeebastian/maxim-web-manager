package com.maxim.spring.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.maxim.spring.dao.ProductDao;
import com.maxim.spring.dto.MiniProduct;
import com.maxim.spring.dto.ProductDTO;
import com.maxim.spring.model.Product;
import com.maxim.spring.model.ProductCategory;

@Service
public class ProductService {

	final String uri = "http://localhost:8083/Central_Manager_Branch/";
	
	@Autowired
	ProductDao productDao;
	
	public void mapFromDTO(ProductDTO productDTO, Product product, ProductCategory category) {
		product.setName(productDTO.getName());
		product.setPrice(new BigDecimal(productDTO.getPrice()));
		product.setProductCategory(category);
		product.setDescription(productDTO.getProductDescription());
		if(productDTO.getHurtPrice()!=null) {
		product.setHurtPrice(new BigDecimal(productDTO.getHurtPrice()));
		} else {
			product.setHurtPrice(new BigDecimal(0));
		}
		product.setQuantity(0);
		
		if(productDTO.getSupplierPrice()!=null) {
			product.setSupplierPrice(new BigDecimal(productDTO.getSupplierPrice()));
			} else {
				product.setSupplierPrice(new BigDecimal(0));
		}
		
	}	
	
	
	
	public void mapExistingFromDTO(ProductDTO productDTO, Product product, ProductCategory category) {
		product.setName(productDTO.getName());
		product.setPrice(new BigDecimal(productDTO.getPrice()));
		product.setProductCategory(category);
		product.setDescription(productDTO.getProductDescription());
	}
	
	public void mapToBranchDTO(ProductDTO productDTO, Product product) {			
		productDTO.setName(product.getName());
		productDTO.setPrice(product.getPrice().toString());
		productDTO.setCategory(product.getProductCategory().getName());
		productDTO.setProductDescription(product.getDescription());
	}
	
	public String generateCredentials() {
		
		String plainCreds = "application:123456";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		return base64Creds;
	}
	
	public void sendToRemote(ProductDTO productDTO, Product product) {
		String base64Creds = generateCredentials();
 		System.out.println(base64Creds);               

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", "Basic " + base64Creds);
		HttpEntity<ProductDTO> entity = new HttpEntity<ProductDTO>(productDTO, headers);
		
		String correctUri = uri + "rest/add-product";
		System.out.println(correctUri);
		try {
		ResponseEntity<String> status = (ResponseEntity<String>) restTemplate.postForEntity( correctUri, entity, String.class);
		product.setSync(true);
		productDao.saveOrUpdate(product);
		}
		catch (final HttpClientErrorException e) {
		product.setSync(false);
		productDao.saveOrUpdate(product);
		System.out.println(e.getStatusCode());
		System.out.println(e.getResponseBodyAsString());
}

	}
	
	public void mapProductsToMiniProducts(List<Product> products, List<MiniProduct> miniProducts) {
		
		for(Product product : products) {
			MiniProduct miniProduct = new MiniProduct();
			miniProduct.setId(product.getId().toString());
			miniProduct.setDescription(product.getDescription());
			miniProduct.setName(product.getName());
			miniProduct.setPrice(product.getPrice());
			miniProduct.setCategory(product.getProductcategory().getName());
			miniProducts.add(miniProduct);
		}
	}
	
//	@Scheduled(fixedDelay = 120000)
//	public void synchronize() {
//		List<Product> productsToSynchronize = productDao.findNotSynchronizedProducts();
//		if(!productsToSynchronize.isEmpty())
//		for(Product product : productsToSynchronize) {
//		ProductDTO productDTO = new ProductDTO();
//		mapToBranchDTO(productDTO, product);
//		sendToRemote(productDTO, product);		
//		} else {
//			System.out.println("NIE MA NIC DO ZSYNCHRONIZOWANIA - CENTRALA");
//		}
//		
//		
//		
//	}
	
	
	public void mapToMiniProduct(Product product, MiniProduct miniProduct) {
		miniProduct.setCategory(product.getProductCategory().getName());
		miniProduct.setDescription(product.getDescription());
		miniProduct.setName(product.getName());
		miniProduct.setPrice(product.getPrice());
		miniProduct.setId(product.getId().toString());
	}
	
// trzeba zmapować na mini produkt, zrobic szyfrowanie haseł - i małe testy
}
