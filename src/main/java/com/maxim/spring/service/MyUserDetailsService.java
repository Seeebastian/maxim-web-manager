package com.maxim.spring.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maxim.spring.dao.UserDao;
import com.maxim.spring.dao.UserDaoImpl;
import com.maxim.spring.dao.UserRoleDao;
import com.maxim.spring.dao.UserRoleDaoImpl;
import com.maxim.spring.model.User;
import com.maxim.spring.model.UserRole;

@Service("myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserDao userDao;
	
	@Autowired 
	private UserRoleDaoImpl userRoleDaoImpl;
	
	@Autowired 
	PasswordEncoder passEncoder;
	@Transactional(readOnly=true)
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User maximUser = userDao.findByUsername(username);
		
		Set<GrantedAuthority> authorities = new HashSet<>();
		
		UserRole userRole = userRoleDaoImpl.getByKey(maximUser.getUserRole().getId());
						
		
		authorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getName()));
		
		return new org.springframework.security.core.userdetails.User(maximUser.getUsername(), maximUser.getPassword(), authorities);
		
	}
	


}
