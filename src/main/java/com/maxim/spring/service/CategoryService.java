package com.maxim.spring.service;


import com.maxim.spring.dto.CategoryDTO;
import com.maxim.spring.model.ProductCategory;

import org.springframework.stereotype.Service;

@Service
public class CategoryService {

	public void mapEntityToDTO(ProductCategory category, CategoryDTO categoryDTO) {
		category.setName(categoryDTO.getName());
		category.setDescription(categoryDTO.getDescription());
	}
	
}
