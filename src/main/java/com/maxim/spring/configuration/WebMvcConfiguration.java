package com.maxim.spring.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableScheduling
@EnableWebMvc
@ComponentScan(basePackages = "com.maxim.spring.*")
public class WebMvcConfiguration extends WebMvcConfigurerAdapter{

	 @Bean
	    public ViewResolver viewResolver() {
	        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	        viewResolver.setViewClass(JstlView.class);
	        viewResolver.setPrefix("/WEB-INF/pages/");
	        viewResolver.setSuffix(".jsp");
	        return viewResolver;
	    }
	 
	    /*
	     * Configure ResourceHandlers to serve static resources like CSS/ Javascript etc...
	     *
	     */
	 
	 	@Override
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {	
	        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
	    }
	 
	    /*
	     * Configure MessageSource to provide internationalized messages
	     *
	     */
	 
	    @Bean
	    public MessageSource messageSource() {
	        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	        messageSource.setBasename("messages");
	        return messageSource;
	    }
	    
	
	
}
