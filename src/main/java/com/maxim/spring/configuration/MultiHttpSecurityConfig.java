package com.maxim.spring.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@EnableWebSecurity
public class MultiHttpSecurityConfig {   
	
			@Autowired
			@Qualifier("myUserDetailsService")
			UserDetailsService userDetailsService;

			@Autowired
			public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
				auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
				//.passwordEncoder(passwordEncoder());
			}
			@Bean
			public PasswordEncoder passwordEncoder(){
				PasswordEncoder encoder = new BCryptPasswordEncoder();
				return encoder;
			}
	
	
	@Configuration
	@Order(2)
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	public static class SecurityConfiguration extends WebSecurityConfigurerAdapter {
		
	//"/rest/**"
		@Override
		protected void configure(HttpSecurity http) throws Exception {

			CharacterEncodingFilter filter = new CharacterEncodingFilter();
	        filter.setEncoding("UTF-8");
	        filter.setForceEncoding(true);
	        http.addFilterBefore(filter,CsrfFilter.class);
			
			http
			.authorizeRequests()
				.antMatchers("/home","/login").permitAll()
		        .antMatchers("/admin/**","/*").access("hasRole('ADMIN') or hasRole('USER')")
		        .antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')")
		        .antMatchers("/app/*").authenticated()
				.and().formLogin().loginPage("/login")
			        .usernameParameter("username").passwordParameter("password")
			        .and().csrf()
			        .and().exceptionHandling().accessDeniedPage("/Access_Denied");
//			
//			 http.authorizeRequests()
//		        
//		        .and().formLogin().loginPage("/login")
//		        .usernameParameter("username").passwordParameter("password")
//		        .and().csrf()
//		        .and().exceptionHandling().accessDeniedPage("/Access_Denied");
		        
		        
		        
		        //defaultAuthenticationEntryPointFor(
//		                loginUrlauthenticationEntryPointWithWarning(),
//		                new AntPathRequestMatcher("/user/private/**"))
//		              .defaultAuthenticationEntryPointFor(
//		                loginUrlauthenticationEntryPoint(), 
//		                new AntPathRequestMatcher("/user/general/**"));
		        
		        
		        //accessDeniedPage("/Access_Denied");
		}
		
	    @Bean
	    public AuthenticationEntryPoint authenticationEntryPoint(){
	        BasicAuthenticationEntryPoint entryPoint = 
	          new BasicAuthenticationEntryPoint();
	        entryPoint.setRealmName("admin realm");
	        return entryPoint;
	    }

	    
	    @Bean
	    public AuthenticationEntryPoint loginUrlauthenticationEntryPoint(){
	        return new LoginUrlAuthenticationEntryPoint("/userLogin");
	    }
	             
	    @Bean
	    public AuthenticationEntryPoint loginUrlauthenticationEntryPointWithWarning(){
	        return new LoginUrlAuthenticationEntryPoint("/userLoginWithWarning");
	    }
	    
	   
		
	
	}
	
	
	@Configuration
	@Order(1)
	public static class SecurityRestConfiguration extends WebSecurityConfigurerAdapter {

		private static String REALM="MY_TEST_REALM";
	 
		
		
		@Override
	    protected void configure(HttpSecurity http) throws Exception {
//	  
//	      http.csrf().disable()
//	        .authorizeRequests()
//	        .antMatchers("/rest/add-products").permitAll()
//	        .and().httpBasic().realmName(REALM).authenticationEntryPoint(getBasicAuthEntryPoint())
//	        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);//We don't need sessions to be created.
	    //hasRole("ADMIN")
			http.antMatcher("/rest/**")                               
			.authorizeRequests()
				.anyRequest().permitAll()
				.and()
			.httpBasic().authenticationEntryPoint(getBasicAuthEntryPoint())
	        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable();//We don't need sessions to be created.
		
		}
	     
	    @Bean
	    public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
	        return new CustomBasicAuthenticationEntryPoint();
	    }
	     
	    /* To allow Pre-flight [OPTIONS] request from browser */
	    @Override
	    public void configure(WebSecurity web) throws Exception {
	        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	    }
	    
	}
	
	
}
