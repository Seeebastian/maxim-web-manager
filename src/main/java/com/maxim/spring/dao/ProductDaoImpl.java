package com.maxim.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maxim.spring.model.Product;
import com.maxim.spring.model.ProductCategory;
import com.maxim.spring.model.User;

@Transactional
@Repository("productDao")
public class ProductDaoImpl extends AbstractDao<Integer, Product> implements ProductDao {

	@Override
	public void save(Product product) {
		super.persist(product);
		
	}
	
	@Override
	public void saveOrUpdate(Product product) {
		super.getSession().saveOrUpdate(product);
	}

	@Override
	public List<Product> findAllProducts() {
		String query = "SELECT c FROM Product c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		List<Product> results = (List<Product>) hibernateQuery.list();
		return results;
	}

	@Override
	public List<Product> findNotSynchronizedProducts() {
		String query = "SELECT c FROM Product c WHERE sync = false";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		List<Product> results = (List<Product>) hibernateQuery.list();
		return results;
	}

	@Override
	public Product findProductWithName(String name) {
		String query = "SELECT c FROM Product c WHERE c.name = :productName";
		Query hibernateQuery = (Query) super.getSession().createQuery(query).setString("productName", name);
		Product result = (Product) hibernateQuery.uniqueResult();
		return result;
	}

	@Override
	public List<Product> findAllProductsLimited() {
		String query = "SELECT c.name, c.price, c.description FROM Product c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		List<Product> results = (List<Product>) hibernateQuery.list();
		return results;
	}

	@Override
	public List<String> findProductsNames() {
		String query = "SELECT c.name FROM Product c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		List<String> results = (List<String>) hibernateQuery.list();
		return results;
		
	}

	@Override
	public void delete(Product product) {
		super.delete(product);
		
	}

	@Override
	public Product findProductWithId(Integer id) {
		String query = "SELECT c FROM Product c WHERE c.id = :id";
		Query hibernateQuery = (Query) super.getSession().createQuery(query).setInteger("id", id);
		Product result = (Product) hibernateQuery.uniqueResult();
		return result;
	}
	
	
	



}
