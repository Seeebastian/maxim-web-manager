package com.maxim.spring.dao;

import com.maxim.spring.model.Shop;
import com.maxim.spring.model.User;



public interface UserDao {
	void saveOrUpdate(User user);
	void save(User user);
	public User findByUsername(String username);
	
}
