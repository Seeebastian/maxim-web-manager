package com.maxim.spring.dao;

import java.util.List;

import com.maxim.spring.model.Product;
import com.maxim.spring.model.Sale;

public interface SaleDao {

	void saveOrUpdate(Sale sale);
	void save(Sale sale);
	List<Sale> findAllSales();
	List<Sale> findAllSalesFromShop(Integer shopId);
	Sale findSaleWithId(Integer id);
	void delete(Sale sale);
	
}
