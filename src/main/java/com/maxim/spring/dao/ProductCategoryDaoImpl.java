package com.maxim.spring.dao;

import java.util.List;
import java.util.Locale.Category;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maxim.spring.model.Product;
import com.maxim.spring.model.ProductCategory;
@Transactional
@Repository("productCategoryDao")
public class ProductCategoryDaoImpl extends AbstractDao<Integer, ProductCategory> implements ProductCategoryDao{

	@Override
	public List<ProductCategory> findAllCategories() {
		String query = "SELECT c FROM ProductCategory c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		List<ProductCategory> results = (List<ProductCategory>) hibernateQuery.list();
		return results;

	}
	
	@Override
	public List<String> findNamesOfAllCategories() {
		String query = "SELECT c.name FROM ProductCategory c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		List<String> results = (List<String>) hibernateQuery.list();
		return results;

	}

	@Override
	public ProductCategory findCategory(String name) {
		String query = "SELECT c FROM ProductCategory c where c.name= :categoryName";
		Query hibernateQuery = (Query) super.getSession().createQuery(query).setString("categoryName", name);
		hibernateQuery.setMaxResults(1);
		ProductCategory result = (ProductCategory) hibernateQuery.uniqueResult();
		return result;
	
	}

	@Override
	public void save(ProductCategory productCategory) {
		super.persist(productCategory);
		
	}
	
	@Override
	public void saveOrUpdate(ProductCategory productCategory) {
		super.getSession().saveOrUpdate(productCategory);
	}
	
	
}
