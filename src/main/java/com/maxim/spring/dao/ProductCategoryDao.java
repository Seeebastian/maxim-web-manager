package com.maxim.spring.dao;

import java.util.List;
import java.util.Locale.Category;

import com.maxim.spring.model.ProductCategory;

public interface ProductCategoryDao {
	List<ProductCategory> findAllCategories();
	List<String> findNamesOfAllCategories();
	ProductCategory findCategory(String name);
	void save(ProductCategory category);
	void saveOrUpdate(ProductCategory category);
}
