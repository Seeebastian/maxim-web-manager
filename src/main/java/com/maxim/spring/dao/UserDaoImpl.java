package com.maxim.spring.dao;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.maxim.spring.model.Shop;
import com.maxim.spring.model.User;


@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@Override
	public User findByUsername(String username) {
		String query = "SELECT u FROM User u Where u.username = :login";
		Query hibernateQuery = (Query) super.getSession().createQuery(query).setString("login", username);
		User user = (User) hibernateQuery.uniqueResult();
		return user;
	}

	@Override
	public void saveOrUpdate(User user) {
		super.getSession().saveOrUpdate(user);
		
	}

	@Override
	public void save(User user) {
		super.persist(user);
		
	}
}
