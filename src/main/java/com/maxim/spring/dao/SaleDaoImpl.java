package com.maxim.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maxim.spring.model.Product;
import com.maxim.spring.model.ProductCategory;
import com.maxim.spring.model.Sale;



@Transactional
@Repository("saleDao")
public class SaleDaoImpl extends AbstractDao<Integer, Sale> implements SaleDao{

	@Override
	public void saveOrUpdate(Sale sale) {
		super.getSession().saveOrUpdate(sale);
		
	}

	@Override
	public void save(Sale sale) {
		super.persist(sale);
	}

	@Override
	public List<Sale> findAllSales() {
		String query = "SELECT c FROM Sale c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		//hibernateQuery.setMaxResults(1);
		List<Sale> result = (List<Sale>) hibernateQuery.list();
		return result;

	}

	@Override
	public List<Sale> findAllSalesFromShop(Integer shopId) {
		String query = "select s from Sale s where s.product in (select p.id from Product p where p.shop = :shopId)";
		Query hibernateQuery = (Query) super.getSession().createQuery(query).setInteger("shopId", shopId);
		//hibernateQuery.setMaxResults(1);
		List<Sale> result = (List<Sale>) hibernateQuery.list();
		return result;

	}

	@Override
	public Sale findSaleWithId(Integer id) {
			String query = "SELECT c FROM Sale c WHERE c.id = :id";
			Query hibernateQuery = (Query) super.getSession().createQuery(query).setInteger("id", id);
			Sale result = (Sale) hibernateQuery.uniqueResult();
			return result;
		
	}

	@Override
	public void delete(Sale sale) {
		super.delete(sale);
	}
	
	
}
