package com.maxim.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maxim.spring.model.Product;
import com.maxim.spring.model.Shop;


@Transactional
@Repository("shopDao")
public class ShopDaoImpl extends AbstractDao<Integer, Shop> implements ShopDao {

	@Override
	public void saveOrUpdate(Shop shop) {
		super.getSession().saveOrUpdate(shop);
		
	}

	@Override
	public void save(Shop shop) {
		super.persist(shop);
		
	}

	@Override
	public List<Shop> findAllShops() {
		String query = "SELECT c FROM Shop c";
		Query hibernateQuery = (Query) super.getSession().createQuery(query);
		//hibernateQuery.setMaxResults(1);
		List<Shop> result = (List<Shop>) hibernateQuery.list();
		return result;
	}

	@Override
	public Shop findShopWithName(String shopName) {
		String query = "SELECT c FROM Shop c WHERE c.name = :shopName";
		Query hibernateQuery = (Query) super.getSession().createQuery(query).setString("shopName", shopName);
		Shop result = (Shop) hibernateQuery.uniqueResult();
		return result;
	}

}
