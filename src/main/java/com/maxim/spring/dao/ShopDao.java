package com.maxim.spring.dao;

import java.util.List;

import com.maxim.spring.model.Sale;
import com.maxim.spring.model.Shop;

public interface ShopDao {

	void saveOrUpdate(Shop shop);
	void save(Shop shop);
	List<Shop> findAllShops();
	Shop findShopWithName(String shopName);
}
