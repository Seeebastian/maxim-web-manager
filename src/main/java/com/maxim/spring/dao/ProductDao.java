package com.maxim.spring.dao;

import java.util.List;

import com.maxim.spring.model.Product;

public interface ProductDao {

	void save(Product product);
	List<Product> findAllProducts();
	List<Product> findAllProductsLimited();
	List<Product> findNotSynchronizedProducts();
	Product findProductWithName(String name);
	void saveOrUpdate(Product product);
	List<String> findProductsNames();
	void delete(Product product);
	Product findProductWithId(Integer id);
}
