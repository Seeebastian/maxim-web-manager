package com.maxim.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@Table(name="product")
@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(nullable=false, length=300)
	private String description;

	@Column(nullable=true, precision=10, scale=2)
	private BigDecimal hurtPrice;

	@Column(nullable=false, length=64)
	private String name;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal price;

	@Column(nullable=false)
	private int quantity;

	@Column(nullable=true, precision=10, scale=2)
	private BigDecimal supplierPrice;
	
	@Column()
	private boolean sync;
	
	@ManyToOne
	@JoinColumn(name="shopId", nullable=false)
	private Shop shop;

	//bi-directional many-to-one association to Productcategory
	@ManyToOne
	@JoinColumn(name="productCategoryId", nullable=false)
	private ProductCategory productcategory;

	public Product() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getHurtPrice() {
		return this.hurtPrice;
	}

	public void setHurtPrice(BigDecimal hurtPrice) {
		this.hurtPrice = hurtPrice;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSupplierPrice() {
		return this.supplierPrice;
	}

	public void setSupplierPrice(BigDecimal supplierPrice) {
		this.supplierPrice = supplierPrice;
	}

	public ProductCategory getProductCategory() {
		return this.productcategory;
	}

	public void setProductCategory(ProductCategory productcategory) {
		this.productcategory = productcategory;
	}

	public boolean isSync() {
		return sync;
	}

	public void setSync(boolean sync) {
		this.sync = sync;
	}
	
	
	public Shop getShop() {
		return this.shop;
	}

	public ProductCategory getProductcategory() {
		return productcategory;
	}

	public void setProductcategory(ProductCategory productcategory) {
		this.productcategory = productcategory;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	


}