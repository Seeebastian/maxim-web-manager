package com.maxim.spring.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale.Category;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.maxim.spring.dao.ProductCategoryDao;
import com.maxim.spring.dao.ProductDao;
import com.maxim.spring.dao.SaleDao;
import com.maxim.spring.dao.ShopDao;
import com.maxim.spring.dto.CategoryDTO;
import com.maxim.spring.dto.MiniProduct;
import com.maxim.spring.dto.ProductDTO;
import com.maxim.spring.dto.TransactionDTO;
import com.maxim.spring.model.Product;
import com.maxim.spring.model.ProductCategory;
import com.maxim.spring.model.Sale;
import com.maxim.spring.model.Shop;
import com.maxim.spring.model.User;
import com.maxim.spring.service.CategoryService;
import com.maxim.spring.service.MyUserDetailsService;
import com.maxim.spring.service.ProductService;
import com.maxim.spring.service.SaleService;

@Controller
public class MainController {

	final String uri = "http://localhost:8083/Central_Manager_Branch/";

	private List<ProductDTO> queue = new ArrayList<>();

	@Autowired
	private ProductCategoryDao productCategoryDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	ProductService productService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	ShopDao shopDao;

	@Autowired
	SaleService saleService;

	@Autowired
	UserDetailsService myUserDetailsService;

	@Autowired
	private SaleDao saleDao;

	@Autowired
	private PasswordEncoder passwordEncoder;
	// Spring Security see this :
	// @RequestMapping(value = "/", method = RequestMethod.GET)
	// public ModelAndView dodajProdukt(ModelMap model) {
	// ModelAndView mav = new ModelAndView("dodaj_produkt");
	// Map<String,String> list = new HashMap<String, String>();
	// list.put(new String("dd"), new String("dds"));
	// list.put(new String("ddd"), new String("dda"));
	// list.put(new String("sd"), new String("ddx"));
	// list.put(new String("dsd"), new String("ddo"));
	// model.addAttribute("categories",list);
	// Categoryy category = new Categoryy();
	//
	// mav.addObject("categories", list);
	// mav.addObject("categoryy",category);
	// return mav;
	// }
	//
	// @RequestMapping(value="/phone-result")
	// private ModelAndView processPhone(@ModelAttribute Categoryy category) {
	// ModelAndView mav = new ModelAndView("dodaj_produkt");
	// mav.addObject("categoryy", category);
	// return mav;
	// }

	@RequestMapping(value = "/add-product", method = RequestMethod.POST)
	public String submitForm(@Valid ProductDTO productDTO, BindingResult result, Model m, HttpServletRequest request) {

		if (result.hasErrors() && request.isUserInRole("ADMIN")) {
			return "dodaj_produkt";
		}
		if (result.hasErrors() && request.isUserInRole("USER")) {
			return "dodaj_produkt_user";
		}

		Shop shop = shopDao.findShopWithName("ZAKOPANE_GUBALOWKA");
		Product product = new Product();
		product.setShop(shop);
		String categoryName = productDTO.getCategory();
		ProductCategory category = productCategoryDao.findCategory(categoryName);
		productService.mapFromDTO(productDTO, product, category);

		// poslanie do modelu listy na nowo po poscie (tej z kategoriami)
		List<String> list = productCategoryDao.findNamesOfAllCategories();
		m.addAttribute("productDTO", new ProductDTO());
		m.addAttribute("categories", list);

		productDao.save(product);

		// String base64Creds = generateCredentials();
		// System.out.println(base64Creds);
		//
		// RestTemplate restTemplate = new RestTemplate();
		// HttpHeaders headers = new HttpHeaders();
		// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		// headers.add("Authorization", "Basic " + base64Creds);
		// HttpEntity<ProductDTO> entity = new
		// HttpEntity<ProductDTO>(productDTO, headers);
		//
		// String correctUri = uri + "rest/add-product";
		// System.out.println(correctUri);
		// try {
		// ResponseEntity<String> status = (ResponseEntity<String>)
		// restTemplate.postForEntity( correctUri, entity, String.class);
		// product.setSync(true);
		// productDao.save(product);
		// }
		// catch (final HttpClientErrorException e) {
		// product.setSync(false);
		// productDao.save(product);
		// System.out.println(e.getStatusCode());
		// System.out.println(e.getResponseBodyAsString());
		// }

		if (request.isUserInRole("ADMIN"))
			return "dodaj_produkt";
		else
			return "dodaj_produkt_user";
	}

	@RequestMapping(value = "/add-product", method = RequestMethod.GET)
	public String getForm(Model m, HttpServletRequest request) {

		List<String> list = productCategoryDao.findNamesOfAllCategories();
		List<String> productsList = productDao.findProductsNames();

		m.addAttribute("categories", list);
		m.addAttribute("products", productsList);
		m.addAttribute("productDTO", new ProductDTO());
		if (request.isUserInRole("ADMIN"))
			return "dodaj_produkt";
		else
			return "dodaj_produkt_user";
	}

	@RequestMapping(value = "/edit-product/{productId}", method = RequestMethod.GET)
	public String editProductGet(Model m, HttpServletRequest request, @PathVariable Integer productId) {

		List<String> list = productCategoryDao.findNamesOfAllCategories();
		List<String> productsList = productDao.findProductsNames();

		Product product = productDao.findProductWithId(productId);

		m.addAttribute("categories", list);
		m.addAttribute("products", productsList);
		m.addAttribute("productDTO", new ProductDTO());
		if (request.isUserInRole("ADMIN")) {
			m.addAttribute("product", product);
			return "edytuj_produkt";
		} else {
			MiniProduct miniProduct = new MiniProduct();
			productService.mapToMiniProduct(product, miniProduct);
			m.addAttribute("miniProduct", miniProduct);
			return "edytuj_produkt_user";
		}
	}

	@RequestMapping(value = "/edit-product", method = RequestMethod.POST)
	public String editProductPost(@Valid ProductDTO productDTO, BindingResult result, Model m,
			HttpServletRequest request) {

		if (result.hasErrors() && request.isUserInRole("ADMIN")) {
			return "dodaj_produkt";
		}
		if (result.hasErrors() && request.isUserInRole("USER")) {
			return "dodaj_produkt_user";
		}

		Shop shop = shopDao.findShopWithName("ZAKOPANE_GUBALOWKA");
		Product product = productDao.findProductWithId(new Integer(productDTO.getId()));
		String categoryName = productDTO.getCategory();
		ProductCategory category = productCategoryDao.findCategory(categoryName);
		productService.mapFromDTO(productDTO, product, category);

		// poslanie do modelu listy na nowo po poscie (tej z kategoriami)
		List<String> list = productCategoryDao.findNamesOfAllCategories();
		m.addAttribute("productDTO", new ProductDTO());
		m.addAttribute("categories", list);

		productDao.saveOrUpdate(product);

		// String base64Creds = generateCredentials();
		// System.out.println(base64Creds);
		//
		// RestTemplate restTemplate = new RestTemplate();
		// HttpHeaders headers = new HttpHeaders();
		// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		// headers.add("Authorization", "Basic " + base64Creds);
		// HttpEntity<ProductDTO> entity = new
		// HttpEntity<ProductDTO>(productDTO, headers);
		//
		// String correctUri = uri + "rest/add-product";
		// System.out.println(correctUri);
		// try {
		// ResponseEntity<String> status = (ResponseEntity<String>)
		// restTemplate.postForEntity( correctUri, entity, String.class);
		// product.setSync(true);
		// productDao.save(product);
		// }
		// catch (final HttpClientErrorException e) {
		// product.setSync(false);
		// productDao.save(product);
		// System.out.println(e.getStatusCode());
		// System.out.println(e.getResponseBodyAsString());
		// }

		if (request.isUserInRole("ADMIN"))
			return "dodaj_produkt";
		else
			return "dodaj_produkt_user";
	}

	@RequestMapping(value = "/add-transaction", method = RequestMethod.GET)
	public String getTransactionForm(Model m, HttpServletRequest request) {

		List<String> list = productCategoryDao.findNamesOfAllCategories();
		List<String> productList = productDao.findProductsNames();

		m.addAttribute("transactionDTO", new TransactionDTO());
		m.addAttribute("products", productList);
		m.addAttribute("categories", list);
		System.out.println("halooooooooooooo:" + request.isUserInRole("ADMIN"));
		if (request.isUserInRole("ADMIN"))
			return "dodaj_transakcje";
		else
			return "dodaj_transakcje_user";
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String news(Model m) {

		return "aktualnosci";
	}

	@RequestMapping(value = "/view-products", method = RequestMethod.GET)
	public String viewProducts(Model m, HttpServletRequest request) {

		List<Product> products;
		products = productDao.findAllProducts();

		// List<String> list = productCategoryDao.findNamesOfAllCategories();
		// List<String> productsList = productDao.findProductsNames();

		// m.addAttribute("categories", list);
		// m.addAttribute("products", productsList);
		m.addAttribute("productDTO", new ProductDTO());

		System.out.println("-------------poczatek");

		if (request.isUserInRole("ADMIN")) {
			System.out.println(products.get(0).getName());
			m.addAttribute("products", products);
		} else {
			System.out.println("teraz tu jestem");
			List<MiniProduct> miniProducts = new ArrayList<>();
			productService.mapProductsToMiniProducts(products, miniProducts);
			m.addAttribute("products", miniProducts);
		}
		System.out.println("-------------koniec");

		if (request.isUserInRole("ADMIN")) {
			return "przegladaj_produkty";
		} else
			return "przegladaj_produkty_user";
	}

	@RequestMapping(value = "/add-transaction", method = RequestMethod.POST)
	public String addTransactionPost(Model m, @Validated TransactionDTO transaction, BindingResult result,
			HttpServletRequest request) {

		if (result.hasErrors() && request.isUserInRole("ADMIN")) {
			return "dodaj_produkt";
		}
		if (result.hasErrors() && request.isUserInRole("USER")) {
			return "dodaj_produkt_user";
		}

		Sale sale = new Sale();
		saleService.mapTransactionDTOtoSale(transaction, sale);

		List<String> list = productCategoryDao.findNamesOfAllCategories();
		List<String> productList = productDao.findProductsNames();

		m.addAttribute("transactionDTO", new TransactionDTO());
		m.addAttribute("products", productList);
		m.addAttribute("categories", list);
		System.out.println("halooooooooooooo:" + request.isUserInRole("ADMIN"));
		if (request.isUserInRole("ADMIN"))
			return "dodaj_transakcje";
		else
			return "dodaj_transakcje_user";

	}

	@RequestMapping(value = "/add-category", method = RequestMethod.GET)
	public String addCategory(Model m) {

		m.addAttribute("categoryDTO", new CategoryDTO());

		return "dodaj_kategorie";
	}

	@RequestMapping(value = "/add-category", method = RequestMethod.POST)
	public String submitCategoryForm(Model m, @Valid CategoryDTO categoryDTO, BindingResult result) {

		if (result.hasErrors()) {
			return "dodaj_kategorie";
		}
		System.out.println("zapisywanie");
		ProductCategory category = productCategoryDao.findCategory(categoryDTO.getName());
		System.out.println("zapisywanie");
		if (category == null) {
			ProductCategory newCategory = new ProductCategory();
			categoryService.mapEntityToDTO(newCategory, categoryDTO);
			System.out.println("zapisywanie");
			productCategoryDao.save(newCategory);
		}

		return "dodaj_kategorie";
	}

	public String generateCredentials() {

		String plainCreds = "application:123456";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		return base64Creds;
	}

	// biiiinder

	// @InitBinder
	// protected void initBinder(WebDataBinder binder) {
	//
	// SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	// dateFormat.setLenient(true);
	// binder.registerCustomEditor(Date.class, new
	// CustomDateEditor(dateFormat,false));
	//
	// }

	@RequestMapping(value = "/view-transactions", method = RequestMethod.GET)
	public String viewTransactions(Model m, HttpServletRequest request) {
		List<Sale> sales;

		Shop shop = shopDao.findShopWithName("ZAKOPANE_GUBALOWKA");
		sales = saleDao.findAllSalesFromShop(shop.getId());
		System.out.println("------");

		m.addAttribute("transactionDTO", new TransactionDTO());

		if (request.isUserInRole("ADMIN")) {
			List<TransactionDTO> transactionsDTO = saleService.mapFromSalesToTransactions(sales, true);
			System.out.println("------");
			System.out.println("------");
			m.addAttribute("sales", transactionsDTO);
			// return "przegladaj_transakcje";
			return "przegladaj_transakcje";
		} else {
			List<TransactionDTO> transactionsDTO = saleService.mapFromSalesToTransactions(sales, false);
			System.out.println("------");
			System.out.println("------");
			m.addAttribute("sales", transactionsDTO);
			return "przegladaj_transakcje_user";
		}
	}

	@RequestMapping(value = "/delete-product", method = RequestMethod.POST)
	public String deleteProduct(Model m, @Validated ProductDTO productDTO, BindingResult result,
			HttpServletRequest request) {

		if (result.hasErrors() && request.isUserInRole("ADMIN")) {
			return "przegladaj_produkty";
		}
		if (result.hasErrors() && request.isUserInRole("USER")) {
			return "przegladaj_produkty_user";
		}

		Product product = productDao.findProductWithId(new Integer(productDTO.getId()));
		productDao.delete(product);

		System.out.println("halooooooooooooo:" + request.isUserInRole("ADMIN"));
		List<Product> products;
		products = productDao.findAllProducts();

		m.addAttribute("productDTO", new ProductDTO());

		if (request.isUserInRole("ADMIN")) {
			System.out.println(products.get(0).getName());
			m.addAttribute("products", products);
		} else {
			List<MiniProduct> miniProducts = new ArrayList<>();
			productService.mapProductsToMiniProducts(products, miniProducts);
			m.addAttribute("products", miniProducts);
		}

		if (request.isUserInRole("ADMIN")) {
			return "przegladaj_produkty";
		} else
			return "przegladaj_produkty_user";

	}

	@RequestMapping(value = "/delete-transaction", method = RequestMethod.POST)
	public String deleteTransaction(Model m, @Validated TransactionDTO transactionDTO, BindingResult result,
			HttpServletRequest request) {

		if (result.hasErrors() && request.isUserInRole("ADMIN")) {
			return "przegladaj_transakcje";
		}
		if (result.hasErrors() && request.isUserInRole("USER")) {
			return "przegladaj_transakcje_user";
		}

		Sale sale = saleDao.findSaleWithId(new Integer(transactionDTO.getId()));
		saleDao.delete(sale);

		m.addAttribute("transactionDTO", new TransactionDTO());

		List<Sale> sales;

		Shop shop = shopDao.findShopWithName("ZAKOPANE_GUBALOWKA");
		sales = saleDao.findAllSalesFromShop(shop.getId());
		System.out.println("------");
		
		System.out.println("------");
		System.out.println("------");


		if (request.isUserInRole("ADMIN")) {
			List<TransactionDTO> transactionsDTO = saleService.mapFromSalesToTransactions(sales,true);
			m.addAttribute("sales", transactionsDTO);
			return "przegladaj_transakcje";
		} else {
			List<TransactionDTO> transactionsDTO = saleService.mapFromSalesToTransactions(sales,false);
			m.addAttribute("sales", transactionsDTO);
			return "przegladaj_transakcje_user";
		}
	}

}
