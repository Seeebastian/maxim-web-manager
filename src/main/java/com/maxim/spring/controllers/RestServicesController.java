package com.maxim.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maxim.spring.dao.ProductCategoryDao;
import com.maxim.spring.dao.ProductDao;
import com.maxim.spring.dto.ProductDTO;
import com.maxim.spring.model.Product;
import com.maxim.spring.model.ProductCategory;
import com.maxim.spring.service.ProductService;

@RestController
public class RestServicesController {

	@Autowired
	private ProductCategoryDao productCategoryDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	ProductService productService;
	
	@RequestMapping(value="/rest/add-product", method = RequestMethod.POST)
	public ResponseEntity<String> addProduct(@RequestBody ProductDTO productDTO ) {
		
		Product product = productDao.findProductWithName(productDTO.getName());
	    System.out.println("-----------------------------------------------");
		String categoryName = productDTO.getCategory();
		ProductCategory category = productCategoryDao.findCategory(categoryName);
		
		if(product==null) {
		 Product newProduct = new Product();   	     
	     productService.mapFromDTO(productDTO, newProduct, category);
	     newProduct.setSync(true);
	     System.out.println("zapis nowego");
	     productDao.save(newProduct); 
		} else {
			productService.mapExistingFromDTO(productDTO, product, category);
			product.setSync(true);
			System.out.println("edycja starego");
			productDao.save(product); 
		}
		
		String status = "success";			
	    System.out.println("Zapisano nowy produkt!");
		
		return new ResponseEntity<String>(status,HttpStatus.OK);			
}
	
	
}
