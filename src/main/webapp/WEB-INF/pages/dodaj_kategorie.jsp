﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>manager</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/my.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">CM</a>
    </div>
    <ul class="nav navbar-nav">
<li><a href="news">Aktualności</a></li>
<li><a href="add-product">Dodaj produkt</a></li>
<li class="active"><a href="add-category">Dodaj kategorie</a></li>
<li><a href="view-products">Przeglądaj produkty</a></li>
<li><a href="add-transaction">Dodaj transakcje</a></li>
<li><a href="view-transactions">Wyświetl transakcje</a>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li id="powitanie">Witaj <sec:authentication property="principal.username" /></li>
      <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Wyloguj</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class = "col-md-12">
        <h3>
          Dodaj kategorie
        </h3>
    </div>
  </div>
  <div class="row">
      <div class="col-md-4">
        <form:form method="POST" action="add-category" commandName="categoryDTO">
          <div class="form-group">
            <form:input path="name" type="text" class="form-control" id="nazwa" placeholder="Nazwa kategorii"/>
          </div>
          <div class="form-group">
            <form:textarea path="description" placeholder="Opis kategorii" class="form-control" rows="5" id="description"></form:textarea>
          </div>
          <button type="submit" class="btn btn-default">Dodaj</button>
        </form:form>
      </div>
  </div>

</div>


<footer class="footer">
    <div class="container">
      <p class="text-muted credit">CM</p>
    </div>
  </footer>


</body>

</html>
