﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>manager</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/my.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">CM</a>
    </div>
    <ul class="nav navbar-nav">
<li><a href="news">Aktualności</a></li>
<li><a href="add-product">Dodaj produkt</a></li>
<li><a href="add-category">Dodaj kategorie</a></li>
<li><a href="view-products">Przeglądaj produkty</a></li>
<li class="active"><a href="add-transaction">Dodaj transakcje</a></li>
<li><a href="view-transactions">Wyświetl transakcje</a>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li id="powitanie">Witaj <sec:authentication property="principal.username" /></li>
      <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Wyloguj</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class = "col-md-12">
        <h3>
          Dodaj transakcje
        </h3>
    </div>
  </div>
  <div class="row">
      <div class="col-md-4">
        <form:form method="POST" action="add-transaction" commandName="transactionDTO">
          <div class="form-group">
            <label for="kategoria">Kategoria produktu</label>
        <div class="form-group">
  			<form:select path="category" class="form-control" items="${categories}" />
        </div>
        </div>
          <div class="form-group">
              <label for="produkt">Produkt</label>
        <div class="form-group">
  			<form:select path="productName" class="form-control" items="${products}" />
        </div>
        </div>
          
	      <div class="form-group">
	      	<label for="discount">Rabat</label>
            <form:input path="discount" type="number" min="0" step="0.1" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="discount" placeholder="discount"/>
          </div>
        
          <div class="form-group">
          <label for="yead">Rok</label>
         <form:select class="form-control" id="year" path="year">
              <form:option value="2017">2017</form:option>
              <form:option value="2018">2018</form:option>
          </form:select>
        </div>
        <div class="form-group">
        <label for="placowka">Miesiąc</label>
         <form:select class="form-control" id="month" path="month">
     	 <option value="01">1</option>
          <option value="02">2</option>
          <option value="03">3</option>
          <option value="04">4</option>
          <option value="05">5</option>
          <option value="06">6</option>
          <option value="07">7</option>
          <option value="08">8</option>
          <option value="09">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          </form:select>
        </div>
        <div class="form-group">
        <label for="day">Dzień</label>
         <form:select class="form-control" id="day" path="day">
       	 <option value="01">1</option>
          <option value="02">2</option>
          <option value="03">3</option>
          <option value="04">4</option>
          <option value="05">5</option>
          <option value="06">6</option>
          <option value="07">7</option>
          <option value="08">8</option>
          <option value="09">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
          </form:select>
        </div>
        
        
        
        <button type="submit" class="btn btn-default">Dodaj</button>
        </form:form>
      </div>
  </div>

</div>


<footer class="footer">
    <div class="container">
      <p class="text-muted credit">CM</p>
    </div>
  </footer>


</body>

</html>
