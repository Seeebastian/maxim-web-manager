﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>manager</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/my.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">CM</a>
    </div>
    <ul class="nav navbar-nav">
<li><a href="news">Aktualności</a></li>
<li><a href="add-product">Dodaj produkt</a></li>
<li><a href="add-category">Dodaj kategorie</a></li>
<li class="active"><a href="view-products">Przeglądaj produkty</a></li>
<li><a href="add-transaction">Dodaj transakcje</a></li>
<li><a href="view-transactions">Wyświetl transakcje</a>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li id="powitanie">Witaj <sec:authentication property="principal.username" /></li>
      <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Wyloguj</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class = "col-md-12">
            <h3>
            Produkty:
            </h3>
        <div class="form-group">
  <label for="myInput">Wpisz kategorie, aby filtrować produkty:</label><br>
    <div class="col-lg-4">
     <input type="text" id="myInput" class="form-control"  onkeyup="myFunction()" placeholder="Kategoria" title="Type in a name">
    </div>
</div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-8 col-md-offset-2">

<table class="table table-hover" id="product-table">
  <thead>
     <tr>
       <th>Nazwa</th>
       <th>Cena</th>
       <th>Opis</th>
       <th>Kategoria</th>
       <th>Akcja</th>
       <th>Edycja</th>
     </tr>
   </thead>
  <c:forEach items="${products}" var="product">
  <thead>
    <tr>
      <form:form method="POST" action="delete-product" commandName="productDTO">
      <form:input path="id" type="hidden" class="form-control" value="${product.id}" id="nazwa" placeholder="Nazwa produktu" />
      <td><c:out value="${product.name}" /></td>
      <td><c:out value="${product.price}" /></td>
      <td><c:out value="${product.description}" /></td>
      <td><c:out value="${product.category}" /></td>
      <td><button type="submit" class="btn btn-default">Usuń</button></td>
      </form:form>
      <td>
      <button class="btn btn-default"><a href="edit-product/${product.id}">Edytuj</a></button>    
      </td>
    </tr>
  </thead>  
  </c:forEach>
</table>
<script>
 function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("product-table");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</div>
</div>
</div>
 

<footer class="footer">
    <div class="container">
      <p class="text-muted credit">CM</p>
    </div>
 </footer>


</body>

</html>
