﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="row">
    <div class="page-header">
      <h1>CM - logowanie</h1>
</div>
<div class="row">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">CM</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="logowanie.html">Logowanie</a></li>
      <li class="active"><a href="rejestracja.html">Rejestracja</a></li>
    </ul>
  </div>
</nav>
</div>
  </div>
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <form id="myForm" action="/action_page_post.php" method="post" onsubmit="return validation()">
      <div class="form-group">
        <input type="text" class="form-control" id="user" placeholder="Użytkownik"  />
      </div>
      <div class="form-group">
        <input type="password" class="form-control" id="pswd" placeholder="Hasło" />
      </div>
      <button class="btn center-block">
        Loguj
      </button>
    </form>
     </div>
  </div>
</div>
<script>
function validation() {
  var x = document.forms["myForm"]["user"].value;
  var y = document.forms["myForm"]["pswd"].value;
  if(x =="") {
    alert("Pola nie mogą być puste!");
    return false;
  } else if (y == "") {
    alert("Pola nie mogą być puste!");
  }
}
</script>
</body>
</html>
