<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>

<head>
	<c:set var="url">${pageContext.request.requestURL}</c:set>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>manager</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/my.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
</head>
<body>

<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">CM</a>
    </div>
    <ul class="nav navbar-nav">
<li><a href="${pageContext.request.contextPath}/news">Aktualności</a></li>
<li><a href="${pageContext.request.contextPath}/add-product">Dodaj produkt</a></li>
<li><a href="${pageContext.request.contextPath}/add-category">Dodaj kategorie</a></li>
<li><a href="${pageContext.request.contextPath}/view-products">Przeglądaj produkty</a></li>
<li><a href="${pageContext.request.contextPath}/add-transaction">Dodaj transakcje</a></li>
<li><a href="${pageContext.request.contextPath}/view-transactions">Wyświetl transakcje</a>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li id="powitanie">Witaj <sec:authentication property="principal.username" /></li>
      <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Wyloguj</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class = "col-md-12">
        <h3>
          Edytujesz <c:out value="${product.name}" />
        </h3>
    </div>
  </div>
  <div class="row">
      <div class="col-md-4">
        <form:form method="POST" action="${pageContext.request.contextPath}/edit-product" commandName="productDTO">
          <form:input path="id" type="hidden" class="form-control" id="nazwa" placeholder="Nazwa produktu" value="${miniProduct.id}" />
          <div class="form-group">
            <form:input path="name" type="text" class="form-control" id="nazwa" placeholder="Nazwa produktu" value="${miniProduct.name}" />
          </div>
          <div class="form-group">
  			<form:select path="category" class="form-control" items="${categories}" value="${miniProduct.category}" />
        </div>
          <div class="form-group">
            <form:textarea path="productDescription" placeholder="Opis produktu" class="form-control" rows="5" maxlength="200" id="description" value="${miniProduct.description}" />
          </div>
            <div class="form-group">
            <form:input path="price" type="number" min="0" step="0.1" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="cena" placeholder="Cena" value="${miniProduct.price}"/>
          </div>
          <button type="submit" class="btn btn-default">Zapisz</button>
        </form:form>
      </div>
  </div>

</div>


<footer class="footer">
    <div class="container">
      <p class="text-muted credit">CM</p>
    </div>
</footer>


</body>

</html>
