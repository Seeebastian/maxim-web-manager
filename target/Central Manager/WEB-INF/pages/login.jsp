﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<title>Web Manager</title>
<meta charset="UTF-8">
</head>
<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
<body>
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>
<div class = "container">
	<div class = "col-lg-4"></div>
		<div class = "col-lg-4">
			<div class = "jumbotron" style="margin-top:150">
			Witamy w Maxim Web Manager. Podaj swoje dane, aby się zalogować.
			 <c:url var="loginUrl" value="/login" />
			<form name='loginForm' action="${loginUrl}"  method='POST'>
			                 <c:if test="${param.error != null}">
                                <div class="alert alert-danger">
                                    <p>Invalid username and password.</p>
                                </div>
                            </c:if>
                             <c:if test="${param.logout != null}">
                                <div class="alert alert-success">
                                    <p>You have been logged out successfully.</p>
                                </div>
                            </c:if>
				<div class = "form-group">
					<input type="text" name="username" class="form-control" placeholder="Login"/>
				</div>
				<div class = "form-group">
					<input type="password" name="password" class="form-control" placeholder="Hasło"/>
				</div>
				<button type="submit" class="btn btn-primary form-control">Zaloguj</button>
			
				  <input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
			</form>
			
			</div>
		</div>
</div>






</body>

</html>